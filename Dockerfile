FROM java:8
ADD target/service-demo-0.0.3-SNAPSHOT.jar /data/service-demo-0.0.3-SNAPSHOT.jar
CMD exec java -Xms64m -Xmx256m -jar -Djava.security.egd=file:/dev/./urandom /data/service-demo-0.0.3-SNAPSHOT.jar
